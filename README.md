# Balboa v0.0.2

## Python 3.x only

Make sure your computer is running Python 3. Depending on how you install and configure it, you'll have to call the correct interpreter for this script.

Example:

`python3 balboa.py ...`

Adding `--version` to the python call will return the version of that interpreter so you can make sure you're calling the
python 3.x interpreter.

```
$ python --version
Python 3.4.1
```

### Setup

Clone to desired directory

`git clone https://github.com/walexnelson/balboa.git`

Install dependencies

`pip install -r requirements.txt`

### Usage

```
-d   : Domo directory
-i   : App Id or list of Ids
-dir : Directory to save metadata files

python balboa.py -d domo.domo.com -i 1560,1561 --dir ~/path/to/save/output
```
