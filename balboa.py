import requests
import argparse
import json
import getpass
import csv
import sys
import os
import logging

from logging.config import fileConfig

cur_dir = os.path.dirname(os.path.realpath(__file__))

# Properties to pull from JSON
PAGE_META = ['pageId', 'type', 'title', 'pageName', 'description']
CARD_META = ['urn', 'title', 'description']
DATA_META = ['id', 'name', 'sourceType', 'dataProvider', 'schedule', 'connectorId']
DATAFLOW_META = ['dataFlowId', 'name', 'databaseType']

ENDPOINTS = {
    'auth': '/api/domoweb/auth/login',
    'app': '/api/library/v1/apps/{}/details'
}


class Template:
    """ Wrapper for a Domo Appstore template """

    def __init__(self, **entries):
        """ Instantiates the template based on JSON entries """

        self.__dict__.update(entries)


class App:
    """ Wrapper for Domo Appstore template """

    def __init__(self, **entries):
        """ Instantiates new app based on JSON entries """

        self.__dict__.update(entries)
        self.base = {
            'appId': self.id,
            'appTitle': self.title,
            'appDescription': self.description,
            'appCustomer': self.customerName
        }

    def writePageMeta(self, base):
        """ Print the page template to csv """

        path, rows = self._prep(base, 'pages.csv')

        if self.pageTemplates:
            for id, tpl in self.pageTemplates.items():
                t = Template(**json.loads(tpl.get('template')))
                row = {key: t.page.get(key) for key in PAGE_META}
                row['cards'] = len(tpl.get('dependencies', None))
                row.update(self.base)
                rows.append(row)

            self._writeCSV(path, rows)

    def writeCardMeta(self, base):
        """" Print the card templates to csv """

        path, rows = self._prep(base, 'cards.csv')
        path = os.path.join(base, str(self.id), 'cards.csv')
        rows = []

        if self.cardTemplates:
            for id, tpl in self.cardTemplates.items():
                t = Template(**json.loads(tpl.get('template')))
                row = {key: t.card.get(key) for key in CARD_META}
                row['dataset'] = tpl.get('dependencies', None)[
                    0].get('sourceId')
                row.update(self.base)
                formulas = json.loads(t.card.get('metadata').get(
                    'formulas')).get('formulas', None)
                rows = rows + self._getBeastModes(formulas, row)

            self._writeCSV(path, rows)

    def writeDatasetMeta(self, base):
        """ Print dataset templates to csv """

        path, rows = self._prep(base, 'datasets.csv')

        if self.dataSourceTemplates:
            for id, tpl in self.dataSourceTemplates.items():
                # Get base template
                t = Template(**json.loads(tpl.get('template')))
                row = {key: t.datasource.get(key) for key in DATA_META}
                row['columns'] = ','.join(
                    [column.get('name', None) for column in t.datasource.get('schema')])
                row.update(self.base)
                formulas = t.datasource.get('beastProperties', None)
                rows = rows + self._getBeastModes(formulas, row)

            self._writeCSV(path, rows)

    def writeDataflowMeta(self, base):
        """ Print dataflow templates to csv """

        path, rows = self._prep(base, 'dataflows.csv')
        if self.dataFlowTemplates:
            for id, tpl in self.dataFlowTemplates.items():
                # Get base template
                t = Template(**json.loads(tpl.get('template')))
                row = {key: t.dataflow.get(key) for key in DATAFLOW_META}
                row.update(self.base)
                actions = t.dataflow.get('actions')
                rows = rows + self._getDataflowActions(actions, row)

            self._writeCSV(path, rows)

    def _getDataflowActions(self, actions, base):
        """ Returns dataflow actions list """

        rows = []
        for action in actions:
            trans = {
                'actionId': action.get('id'),
                'actionType': action.get('type')
            }
            if action.get('type') == 'SQL':
                trans['sql'] = action.get('statements')[0]
            elif action.get('type') == 'PublishToVault':
                trans['sql'] = action.get('query')
            else:
                trans['sql'] = 'NA'

            trans.update(base)
            rows.append(trans)

        return rows

    def _getBeastModes(self, formulas, base):
        """ Returns beastmodes from card or dataset template """

        rows = []
        if formulas:
            for id, fn in formulas.items():
                bm = {
                    'bmId': fn.get('id', None),
                    'bmName': fn.get('name', None),
                    'bmFormula': fn.get('formula', None)
                }
                bm.update(base)
                rows.append(bm)
        else:
            bm = {'bmId': None, 'bmName': None, 'bmFormula': None}
            bm.update(base)
            rows.append(bm)

        return rows

    def _prep(self, base, filename):
        """ Returns standard path and file """

        return os.path.join(base, str(self.id), filename), []

    def _writeCSV(self, file, data):
        """ Writes data to provided file """

        with open(file, 'w') as f:
            w = csv.DictWriter(f, fieldnames=data[0].keys(
            ), delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
            w.writeheader()
            w.writerows(data)


class Domo:
    """ Interact with Domo API """

    def __init__(self, domain):
        """ Set domain """

        self.domain = 'https://{}'.format(domain)
        self.session = requests.Session()

    def _build_url(self, var):
        """ Helper function to build url """

        return '{}{}'.format(self.domain, ENDPOINTS.get(var))

    def login(self, usr, pwd):
        """ Authenticate """

        payload = json.dumps({'username': usr, 'password': pwd})
        r = self.session.post(self._build_url('auth'), data=payload)

        auth = {'domo-domo-authentication': r.json().get('sid')}
        self.session.headers.update(auth)
        return r.status_code == 200

    def get_app(self, app_id):
        """ Return Appstore App by ID """

        url = self._build_url('app').format(app_id)
        params = {'includeHistory': False, 'includeTemplates': False}
        r = self.session.get(url, params=params)

        return App(**r.json())


def make_dir(root, name):
    """ Make directory if not exists """

    full_path = os.path.join(root, name)
    if not os.path.exists(full_path):
        os.makedirs(full_path)


def build_args():
    """ Helper function """

    output_dir = os.path.join(cur_dir, 'dist')

    parser = argparse.ArgumentParser("Balboa v0.0.2")
    parser.add_argument('-d', '--domo', required=True)
    parser.add_argument('-a', '--apps', required=True)
    parser.add_argument('--dir', default=output_dir)

    return parser.parse_args()


def prompt_user():
    """ user prompts """

    usr = input("Username: ")
    pwd = getpass.getpass()
    return usr, pwd


if __name__ == "__main__":

    fileConfig('log_config.ini')
    logger = logging.getLogger(__name__)

    args = build_args()
    usr, pwd = prompt_user()
    domo = Domo(args.domo)

    # If successful login
    if domo.login(usr, pwd):
        for a in [x.strip() for x in args.apps.split(',')]:
            app = domo.get_app(a)
            make_dir(args.dir, str(app.id))

            logger.info('APP {}: WRITING PAGE META'.format(app.id))
            app.writePageMeta(args.dir)
            logger.info('APP {}: WRITING CARD META'.format(app.id))
            app.writeCardMeta(args.dir)
            logger.info('APP {}: WRITING DATASET META'.format(app.id))
            app.writeDatasetMeta(args.dir)
            logger.info('APP {}: WRITING DATAFLOW META'.format(app.id))
            app.writeDataflowMeta(args.dir)
            logger.info('APP {}: COMPLETE'.format(app.id))
    else:
        logger.error('Unauthorized. Check your credentials')
